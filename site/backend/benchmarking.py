import os
from parser import getCompilers
from utils import cd, logger, config, repos
from subprocess import Popen, PIPE, STDOUT, TimeoutExpired, SubprocessError
from htmlgen import generate_test_results
from timeout import timeoutdec
from pathlib import Path
osp = os.path

class Benchmarker:
    default_timeout = int(config['DEFAULT']['timeout'])

    def __init__(self):
        logger.debug('Initializing Benchmarker instance...')

        self.compiler_install_dir = Path(osp.expanduser('~/.local/bin'))

        self.compiler_repo_url = str(config['REPOS']['compiler_repo'])
        self.benchmarks_repo_url = str(config['REPOS']['benchmark_repo'])
        self.compiler_repo_path = Path(config['PATHS']['compiler_path'])
        self.benchmarks_repo_path = Path(config['PATHS']['benchmarks_path'])
        self.results_path = Path(config['PATHS']['results_path'])
        self.runs = int(config['DEFAULT']['runs'])

        self.installed = False

    def __del__(self):
        self.cleanup()

    @property
    def compiler_repo(self):
        return repos.compiler_repo

    @property
    def compiler_commit(self):
        return self.compiler_repo.head.commit

    @property
    def benchmarks_repo(self):
        return repos.benchmarks_repo

    @property
    def benchmarks_commit(self):
        return self.benchmarks_repo.head.commit

    @timeoutdec(default_timeout)
    def setup(self):
        logger.info('Building/installing Futhark...')
        try:
            with cd(str(self.compiler_repo_path)):
                cmds = (['stack', 'setup'],
                        ['stack', 'build'],
                        ['stack', 'install'])
                for cmd in cmds:
                    with Popen(cmd, stderr=STDOUT, universal_newlines=True) as proc:
                        proc.wait(timeout=self.default_timeout)
                        if proc.returncode != 0:
                            raise SubprocessError
            logger.info('Successfully built and installed Futhark.')
            self.installed = True
        except (TimeoutExpired, TimeoutError):
            logger.warning('Timeout expired, building/installing Futhark took too long. Exiting...')
            self.cleanup()
        except SubprocessError:
            logger.warning('Build/install of Futhark compilers failed. Exiting...')
            self.cleanup()

    def benchmark(self, runs=None):
        if runs is not None:
            self.runs = runs

        self.cleanup()
        repos.fetch_repos()
        self.setup()

        self._benchmark()

    @timeoutdec(7200) # timeout ALL benchmarks after 2 hours
    def _benchmark(self):
        logger.info('Running benchmarks...')
        for testfile in self.benchmarks_repo_path.glob('**/*.fut'):
            # file paths
            relroot = str(testfile.parent).replace(str(self.benchmarks_repo_path), '').lstrip('/')

            # is this test to be benchmarked?
            binaries = [b.name for b in self.compiler_install_dir.iterdir() if
                        b.name.startswith('futhark') and
                        b.is_file()]
            compilers = getCompilers(str(testfile), binaries)
            if not compilers:
                continue

            # actual benchmarking
            for c in compilers:
                newroot = self.results_path / c / str(self.compiler_commit) / str(self.benchmarks_commit) / relroot
                resfile = newroot / str(testfile.name).replace('.fut', '.txt')

                if not newroot.exists():
                    newroot.mkdir(parents=True)

                if resfile.exists():
                    htmlfile = Path(str(resfile).replace('.txt', '.html'))
                    if not htmlfile.exists():
                        generate_test_results(str(resfile.absolute()), 'templates/static_test_template.html')

                    logger.info('{} exists. Skipping...'.format(str(resfile)))
                    continue

                # run benchmark
                cmd = [str(self.compiler_install_dir / 'futhark-bench'),
                       str(testfile), '--runs=' + str(self.runs),
                       '--compiler=' + str(self.compiler_install_dir / c), '--raw']
                if not config['DEFAULT'].getboolean('validate_results'):
                    cmd.append('-n')
                try:
                    logger.info('Running benchmark {} with the {} compiler over {} runs'.format(str(testfile), c, self.runs))
                    with Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, universal_newlines=True) as proc:
                        # write output to file
                        stdout, stderr = proc.communicate(timeout=self.default_timeout)
                        with resfile.open('w') as f:
                            f.write(str(proc.returncode) + '\n' + (stdout if proc.returncode == 0 else stderr))
                except (TimeoutExpired, TimeoutError):
                    logger.warning('Running benchmark ' + str(testfile) + ' with ' + c + ' took too long. Moving on...')
                    continue

                generate_test_results(str(resfile.absolute()), 'templates/static_test_template.html')

        logger.info('Finished running benchmarks.')

    def cleanup(self):
        repos.cleanup()

        self.installed = False

benchmarker = Benchmarker()


if __name__ == '__main__':
    benchmarker.benchmark(10)
