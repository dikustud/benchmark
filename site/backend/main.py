import argparse, http.server, os, sys
from benchmarking import benchmarker
from utils import logger, config

pid = str(os.getpid())
pidfile = '/tmp/benchmarking-infrastructure.pid'

def main():
    logger.debug('Initializing program...')

    parser = argparse.ArgumentParser(description='Run Futhark benchmarks and generate test output data and graphs')
    parser.add_argument('--runs', type=int, help='Number of runs over which the futhark-bench binary will measure the tests')
    parser.add_argument('--no-http', action='store_true', help='If --no-http is used it will not enable the HTTP server and only run benchmarks and subsequent tasks')
    parser.add_argument('--address', type=str, help='IP address to listen on if --no-http is not set. Default is localhost (or defined in config.ini)')
    parser.add_argument('--port', type=int, help='The port number to listen on if --no-http is not set. Default is 9009 (or defined in config.ini)')
    args = parser.parse_args()

    if os.path.isfile(pidfile):
        try:
            os.kill(int(open(pidfile).read()), 0)
        except OSError:
            os.remove(pidfile)
        else:
            logger.info('{} already exists, exiting...'.format(pidfile))
            sys.exit()

    open(pidfile, 'w').write(pid)

    runs = args.runs if args.runs else int(config['DEFAULT']['runs'])
    no_http = args.no_http if args.no_http else config['DEFAULT'].getboolean('no_http')
    address = args.address if args.address else str(config['DEFAULT']['address'])
    port = args.port if args.port else int(config['DEFAULT']['port'])

    logger.debug('Port: {}. Address: {}. No-http: {}. Runs: {}.'.format(port, address, no_http, runs))

    class ServerHandler(http.server.BaseHTTPRequestHandler):
        def do_POST(self):
            logger.info('Request received. Running benchmarks...')
            benchmarker.benchmark(runs)
    if no_http:
        logger.info('No-http enabled. Running benchmarks...')
        benchmarker.benchmark(runs)
    else:
        target = (address, port)

        logger.info('HTTP server listening on ' +
                    ':'.join([str(x) for x in target]) + '...')
        httpd = http.server.HTTPServer(target, ServerHandler)
        httpd.serve_forever()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logger.warning('Killed by user KeyboardInterrupt. Exiting...')
        sys.exit(1)
