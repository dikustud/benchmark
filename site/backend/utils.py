import os, shutil, git, configparser, logging, sys
from timeout import timeoutdec
osp = os.path

config_path = osp.join(osp.dirname(osp.realpath(__file__)), 'config.ini')

config = configparser.ConfigParser()
config.read(config_path)

log_path = osp.abspath(config['PATHS']['log_path'])
stdout = logging.StreamHandler(sys.stdout)
stdout.setLevel(getattr(logging, str(config['DEBUG']['stdout_debug_level']).upper()))
stderr = logging.StreamHandler(sys.stderr)
stderr.setLevel(getattr(logging, str(config['DEBUG']['stderr_debug_level']).upper()))
log = logging.FileHandler(log_path)
log.setLevel(getattr(logging, str(config['DEBUG']['log_debug_level']).upper()))
logging.basicConfig(format='[%(asctime)s] - {%(levelname)s:%(filename)s:%(lineno)d} - %(message)s', level=logging.DEBUG, handlers=[stdout, stderr, log])
logger = logging.getLogger('utils')

def getLatestCommit(url):
    """Returns the latest (long format) commit of the repo pointed to by url"""
    g = git.cmd.Git()
    head = g.ls_remote(url, 'HEAD').replace('HEAD', '').strip()
    return head

def sortCommits(commit_list, repo):
    """
    Takes a (long format) commit_list, and returns it sorted,
    newest to oldest
    """
    commits = [c.split()[0] for c in repo.git.log('--pretty=oneline').split('\n')]
    return [c for c in commits if c in commit_list]

# Thank you Brian M. Hunt
# http://stackoverflow.com/questions/431684/how-do-i-cd-in-python/13197763#13197763
class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class RepoLoader:
    default_timeout = int(config['DEFAULT']['timeout'])

    def __init__(self):
        self.compiler_repo_url = str(config['REPOS']['compiler_repo'])
        self.benchmarks_repo_url = str(config['REPOS']['benchmark_repo'])
        self.compiler_repo_path = osp.abspath(config['PATHS']['compiler_path'])
        self.benchmarks_repo_path = osp.abspath(config['PATHS']['benchmarks_path'])

        self.cleanup()

        self._compiler_repo = None
        self._benchmarks_repo = None

    def __del__(self):
        self.cleanup()

    @timeoutdec(default_timeout)
    def fetch_repos(self):
        try:
            self.cleanup()
            logger.info('Cloning Futhark repositories...')
            self._compiler_repo = git.Repo.clone_from(self.compiler_repo_url, self.compiler_repo_path)
            self._benchmarks_repo = git.Repo.clone_from(self.benchmarks_repo_url, self.benchmarks_repo_path)
            logger.info('Successfully cloned Futhark repositories.')
        except TimeoutError:
            logger.warning('Cloning a Futhark repository took too long. Exiting...')
            self._compiler_repo = None
            self._benchmarks_repo = None

    @property
    def compiler_repo(self):
        if self._compiler_repo is None:
            self.fetch_repos()
        return self._compiler_repo

    @property
    def benchmarks_repo(self):
        if self._benchmarks_repo is None:
            self.fetch_repos()
        return self._benchmarks_repo

    def cleanup(self):
        if osp.exists(self.benchmarks_repo_path):
            shutil.rmtree(self.benchmarks_repo_path)

        if osp.exists(self.compiler_repo_path):
            shutil.rmtree(self.compiler_repo_path)

        self._benchmarks_repo = None
        self._compiler_repo = None

repos = RepoLoader()
