import re, os
from pathlib import Path, PurePath
from utils import config

def getResultsPath():
    """Returns the results path from the config"""
    return os.path.abspath(config['PATHS']['results_path'])

def getTestName(path):
    """Returns the name of the test/path"""
    return(os.path.splitext(os.path.basename(path))[0])

def getOutputName(path):
    """Returns the name of the output file"""
    return os.path.basename(path)

def getBenchmarkCommitName(path):
    """Returns the benchmark commit name"""
    n = len(getResultsPath().split(os.sep))
    p = PurePath(path)
    return p.parts[n+2] # We know that the benchmark commit is the third after the results path

def getCommitName(path):
    """Returns the commit name"""
    n = len(getResultsPath().split(os.sep))
    p = PurePath(path)
    return p.parts[n+1] # We know that the commit name is the second after the results path

def getCompilerName(path):
    """Returns the compiler name"""
    n = len(getResultsPath().split(os.sep))
    p = PurePath(path)
    return p.parts[n] # We know that the compiler name is right after the results path

def getTags(path):
    """Returns all the information in the tags field"""
    with open(path) as f:
        for line in f:
            m = re.match('-- tags {(.*)}', line)
            if m:
                return m.group(1).split()
    return []

def getCompilers(path, compiler_list):
    """Returns a list of compilers from a path and compares them to a compiler list"""
    tags = getTags(path)
    return [c for c in compiler_list if c in tags]

def parseDataset(string):
    """Helper function for getOutput, returns the dataset names"""
    m = re.match('dataset (.*)', string)
    return m.group(1).split()

def getOutput(path):
    """Returns the output of a path, by parsing the different lines"""
    res = []
    with open(path) as f:
        for line in f:
            if line.startswith('0'):
                res.append(True)
            elif line.startswith('1'):
                res.append(False)
            elif line.startswith('dataset'):
                res.append(parseDataset(line))
    return res

def getDatasets(path):
    """Returns all datasets and their run values"""
    return getOutput(path)[1:]

def getFirstDataset(path):
    """Returns the first dataset"""
    output = getOutput(path)
    return output[1] if len(output) > 1 else ''

def getRunsAmount(path):
    """Returns the amount of runs, by taking the length of the first dataset"""
    return len(getFirstDataset(path))-1

def getDatasetAmount(path):
    """Takes a path and returns the amount of different datasets"""
    output = getOutput(path)
    datasets = len(output)-1
    return datasets

def getDatasetNames(path):
    """Returns the names of the datasets"""
    datasets = getDatasets(path)
    return [d[0] for d in datasets]

def getAvg(path):
    """Gets the average of each dataset given from the path"""
    datasets = getDatasets(path)
    res = []
    runs = getRunsAmount(path)
    for dataset in datasets:
        y = 0.0
        for x in range(1, runs+1):
            y += int(dataset[x])
        y = y / runs
        res.append(y)
    return res

def getAllOutputs(path):
    """Returns all paths to the same test from the same compiler"""
    resultsPath = getResultsPath()
    test = getOutputName(path)
    compiler = getCompilerName(path)
    return [str(o.absolute())
            for o in Path(resultsPath, compiler).glob('**/'+test)]

def getAllTestPages(path):
    """Returns all paths to tests from the same compiler, commit and benchmark commit"""
    resultsPath = getResultsPath()
    compiler = getCompilerName(path)
    commit = getCommitName(path)
    bcommit = getBenchmarkCommitName(path)
    return [str(t.absolute())
            for t in Path(resultsPath, compiler, commit, bcommit).glob('**/*.txt')]

def getAllCompilerPages(path):
    """Returns all paths to compilers pages from the same commit and benchmark commit"""
    resultsPath = getResultsPath()
    compiler = getCompilerName(path)
    commit = getCommitName(path)
    bcommit = getBenchmarkCommitName(path)
    return [str(b.absolute()) for a in Path(resultsPath).glob('*')
            for b in Path(a, commit, bcommit).glob('*.html')]

def getAllCommits(paths):
    """Returns all the commit names"""
    return [getCommitName(p) for p in paths]

def getAllAvgs(paths):
    """Returns the average of the datasets from the same test (And its older commits)"""
    return [getAvg(p) for p in paths]

def generateCPath(path):
    """Generates the path to the compiler results page"""
    resultsPath = getResultsPath()
    commit = getCommitName(path)
    bcommit = getBenchmarkCommitName(path)
    compiler = getCompilerName(path)
    return str(Path(resultsPath, compiler, commit, bcommit, compiler + '.html').absolute())

def getValidResults(paths):
    """Takes a list of paths, returns a list of paths that all have valid results"""
    return [p for p in paths if getAvg(p)]

def sortOutputs(commit_list, paths):
    """Returns output paths sorted according to commits"""
    sorted_outputs = [None] * len(commit_list)
    for path in paths:
        try:
            tmp_commit = getCommitName(path)
            i = commit_list.index(tmp_commit)
            sorted_outputs[i] = path
        except ValueError:
            pass
    return sorted_outputs
