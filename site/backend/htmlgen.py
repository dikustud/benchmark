import sys, os
import matplotlib.pyplot as plt
from lxml import html, etree
from parser import *
from utils import config, logger, sortCommits, repos
from pathlib import Path

def generate_test_results(path, template):
    """Generates a static webpage for a given test"""
    cPath = generateCPath(path)
    generate_compiler_results(cPath, 'templates/static_compiler_template.html') # Generate compiler page

    logger.debug('Starting HTML Generator for test results...')
    logger.debug('Trying to create HTML page for: {}.'.format(path))
    tree = html.parse(template)
    #tree.docinfo.clear() # this does not work on all versions of lxml

    root = tree.getroot()

    # fill in name(s)
    names = root.findall('.//*[@class="test_name"]')
    compilerName = getCompilerName(path)
    for n in names:
        n.text = getTestName(path) + ' (' + compilerName + ')'

    # Add the generated compiler page to the page
    compiler = root.find('.//*[@id="compiler_list"]')
    c = getCompilerName(path)
    li = etree.Element('li')
    a = etree.Element('a')
    a.attrib['href'] = str(Path(cPath).relative_to(getResultsPath()))
    a.text = c
    li.append(a)
    compiler.append(li)

    # Check whether the test failed or not, and write a message corresponding to the code on the page
    return_code = getOutput(path)[0]
    success = root.find('.//*[@class="success"]')
    success.text = 'SUCCESS' if return_code else 'FAILED'
    logger.debug('Found return code: {}.'.format(return_code))

    # find table and header_row
    table = root.find('.//table[@id="results"]')
    header_row = root.find('.//tr[@id="header_row"]')

    # Only fill in data if test is successful
    if return_code:
        # number of runs and datasets
        runs_amount = getRunsAmount(path)
        dataset_amount = getDatasetAmount(path)
        logger.debug('Number of runs found: {}.'.format(runs_amount))
        logger.debug('Number of datasets found: {}.'.format(dataset_amount))

        datasets = getDatasets(path)

        # fill in results
        avg = getAvg(path)
        for x in range(dataset_amount): # number of results (rows)
            row = etree.Element('tr')
            dataset = etree.Element('td')
            dataset.text = datasets[x][0]
            row.append(dataset)

            for y in range(0,runs_amount+1): # number of runs
                if x == 0: # only add headers once!
                    run = etree.Element('th')
                    if y == runs_amount:
                        run.text = 'Average'
                    else:
                        run.text = 'Run ' + str(y)
                    header_row.append(run)

                column = etree.Element('td')
                if y == runs_amount:
                    column.text = str(format(round(avg[x],2)))
                else:
                    column.text = datasets[x][y+1] + ' us' # +1 because 0 is the name
                row.append(column)
                table.append(row)

        # Generate and insert graph
        logger.debug('Trying to build and insert graph...')
        graph_path = graph_builder(path)
        graph = etree.Element('img')
        graph.attrib['src'] = os.path.basename(graph_path)
        root.find('.//*[@class="graph"]').append(graph)
        logger.debug('Successfully built and inserted graph.')
    else: # change colors to red because test failed
        for th in table.findall('.//th'):
            th.attrib['style'] = 'background-color:red'

    logger.debug('Trying to save HTML to file...')
    savePath = os.path.splitext(path)[0] + '.html'
    with open(savePath, 'w') as f:
        f.write(etree.tostring(root, pretty_print=True).decode('utf-8'))
    logger.debug('Successfully saved the webpage to: {}.'.format(savePath))

def generate_compiler_results(path, template):
    """Generate a static page for compilers"""
    logger.debug('Starting HTML Generator for compiler results...')
    logger.debug('Trying to create HTML page for: {}.'.format(path))

    tree = html.parse(template)
    root = tree.getroot()

    commit = getCommitName(path)[:7]
    bCommit = getBenchmarkCommitName(path)[:7]

    # Insert links to the commit changes on the repository pages (GitHub)
    commit_id = root.find('.//*[@id="commit"]')
    l = etree.Element('a')
    l.attrib['href'] = str(config['REPOS']['compiler_url']) + 'commit/' + commit
    l.attrib['target'] = '_blank'
    l.text = 'commit: ' + commit
    commit_id.append(l)
    bcommit_id = root.find('.//*[@id="bcommit"]')
    l = etree.Element('a')
    l.attrib['href'] = str(config['REPOS']['benchmark_url']) + 'commit/' + bCommit
    l.attrib['target'] = '_blank'
    l.text = 'benchmark commit: ' + bCommit
    bcommit_id.append(l)

    pages = getAllTestPages(path)

    table = root.find('.//*[@id="results"]')

    # Insert all the links to benchmark results
    for p in pages:
        test = getTestName(p)
        row = etree.Element('tr')
        data = etree.Element('td')
        link = etree.Element('a')
        path_to_bcommit = Path(getResultsPath(), getCompilerName(path), getCommitName(path), getBenchmarkCommitName(path)).absolute()
        link.attrib['href'] = str(Path(os.path.splitext(p)[0] + '.html').relative_to(path_to_bcommit))
        link.text = test
        data.append(link)
        row.append(data)
        table.append(row)

    savePath = generateCPath(path)
    with open(savePath, 'w') as f:
        f.write(etree.tostring(root, pretty_print=True).decode('utf-8'))

    logger.debug('Successfully saved the webpage to: {}.'.format(savePath))

def graph_builder(path):
    """Builds a graph from a given path to a test output. Returns the path to the graph that has been built"""
    log_bool = config['GRAPH'].getboolean('log_scale')
    num_tests = int(config['GRAPH']['num_tests'])
    d_limit_chars = int(config['GRAPH']['dataset_limit_chars'])

    logger.debug('Building a graph for path: {}.'.format(path))
    outputs = getValidResults(getAllOutputs(path)) # Get all outputs from a given test and then remove all invalid test results

    if not outputs: # If there are no valid paths, exit program
        logger.warning('There are no valid paths, cannot create a graph. Exiting...')
        return

    commits = getAllCommits(outputs)
    commits = sortCommits(commits, repos.compiler_repo)[:num_tests] # Sort commits
    commits = [c for c in reversed(commits)] # Reverse the order as GitHub lists newest commit first, and we want the oldest first

    outputs = sortOutputs(commits, outputs)
    avgs = getAllAvgs(outputs)

    datasets = getDatasetNames(outputs[-1])
    testName = getTestName(outputs[-1])
    dataset_len = getDatasetAmount(outputs[-1])
    savePath = os.path.abspath(os.path.splitext(outputs[-1])[0] + '.png') # Path to save the file
    logger.debug('Test is: {}.'.format(testName))
    logger.debug('Commits found: {}.'.format(commits))
    logger.debug('Datasets found: {}.'.format(datasets))
    logger.debug('Dataset length: {}'.format(dataset_len))
    logger.debug('Outputs found: {}'.format(outputs))


    logger.debug('Average is: {}'.format(avgs))

    x = []
    try:
        logger.debug('Trying to create plot points...')
        # Create a list that goes from 0 to the length of outputs
        for m in range(0, len(outputs)):
            x.append(m)

        # Limit dataset names to XX characters
        for j in range(0, len(datasets)):
            datasets[j] = datasets[j][:d_limit_chars]

        # Limit commit names to 7 characters
        for i in range(0, len(commits)):
            commits[i] = commits[i][:7]

        # Fill in plots
        for n in range(0, dataset_len):
            y = []
            # Fill in averages into a list for the y-axis
            for avg in avgs:
                y.append(avg[n])
            plt.xticks(x, commits, rotation=70) # Change x-axis to contain commit names instead of 0 to length of outputs
            logger.debug('x is: {} and y is: {}'.format(x,y))
            plt.plot(x, y, 'o-') # Insert plot into graph
        logger.debug('Successfully created plot points.')
    except ValueError:
        logger.warning('Number of outputs: {}. Number of datasets: {}.\nValueError in creating plot points, likely due to number of x and y values not matching. Exiting...'.format(len(outputs), dataset_len))
        return

    except IndexError:
        logger.warning('Averages of outputs: {}.\nIndexError in creating plots, likely due to parts of the list being empty. Exiting...'.format(avgs))
        return

    try:
        logger.debug('Trying to save the graph...')
        plt.ylabel('Build/run time in microseconds')
        if log_bool: # If log_bool has been set to True in the config
            plt.yscale('log')
        plt.xlabel('Commit')
        lgd = plt.legend(datasets, bbox_to_anchor=(1, 0.5), loc='center left', fancybox=True)
        plt.title(testName)
        plt.grid(True)
        plt.tight_layout()
        plt.savefig(savePath, bbox_extra_artists=(lgd,), bbox_inches='tight') # Save the graph
        logger.debug('Succesfully saved the graph to: {}.'.format(savePath))
        plt.clf() # Remember to clear the plots otherwise they will carry onto the next graph!
        return savePath # Return the path to the graph as the test page generator needs to embed the graph
    except:
        logger.warning('Could not save the graph. Exiting...')

if __name__ == "__main__":
    graph_builder("/home/cralle/DIKUSTUD/Benchmark/site/backend/results/futhark-opencl/4acabca06fd32265a19122c11b6b8d28948fc0ab/715e3a3cac185fbd0dea1664227398535f14ccb6/accelerate/nbody/nbody.txt")
