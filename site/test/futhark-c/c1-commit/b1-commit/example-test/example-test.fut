-- Nedenstående er eksempel på hvordan vi kunne opstille tests
-- For at vise om den skal compiles og hvordan
--
-- ==
-- tags { futhark-c futhark-py XYZ JFAOJ }
-- compiled input {
--   20
--   30
--   40
-- }
-- compiled output {
--   40
--   60
--   80
-- }