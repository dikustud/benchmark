\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper, hmargin={2.8cm, 2.8cm}, vmargin={2.5cm, 2.5cm}]{geometry}
\usepackage{eso-pic}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{nameref}
\def \ColourPDF {../ku-template/include/ku-farve}
\def \TitlePDF {../ku-template/include/ku-en}
\newcommand{\source}[1]{\caption*{Source: {#1}} }
\title{
  \vspace{3cm}
  \Huge{Benchmark Infrastructure} \\
  \Large{Midway Report}
}
\date{
    \today
}
\author{
	\Large{Lasse Halberg Haarbye} - \textbf{lpt113}
	\and
	\Large{Christian E.N. Hansen} - \textbf{vmk541}
}

\begin{document}
%% Frontpage magic
\AddToShipoutPicture*{\put(0,0){\includegraphics*[viewport=0 0 700 600]{\ColourPDF}}}
\AddToShipoutPicture*{\put(0,602){\includegraphics*[viewport=0 600 700 1600]{\ColourPDF}}}
\AddToShipoutPicture*{\put(0,0){\includegraphics*{\TitlePDF}}}
\clearpage\maketitle
\thispagestyle{empty}
\newpage

\section{Abstract}
This report will show how we implemented an benchmarking infrastructure for a functional programming language called Futhark, this infrastructure will show the quality of the programming language  compared to previous revisions. We want to create ongoing reports of the quality of Futhark, and our problem is how do we create such an infrastructure. Through our experiences and analysis we concluded that we would use a single unit build/run time to compare the performances. We created the infrastructure in \textit{Python}, and create static webpages for each test and compiler. 

\section{Introduction}
When developing a larger piece of software, how do you as a developer keep track on your softwares performance? It can be tiring and take up a lot of time, that could be spent wiser. \\ 
The aim of this project is to create a solution for the problem: "How do you keep track of the quality of a compiler overtime?". And also how do you do it without the need for human interaction. The solution has to show the overall performance overtime, and should be able to run these tests automatically. \\ \\
Our solution to this problem, is to create benchmark reports overtime. These reports will show how a given compiler is doing compared to its earlier revisions. To do this we will run test programs, and measure how long it takes for the test to build and run. We have written this benchmarking infrastructure for functional programming language \textit{Futhark}, which is still being worked on, it is therefore important that we make sure our infrastructure will not break under future expansions. \\ \\
We have written a infrastructure in programming language \textit{Python}, which will run whenever we see a new revision of Futhark being committed. We will run tests on some of Futharks exsisting tests, document the performance of the test and its compiler with static webpages, that will show the overall quality of a compiler.
\\ \\
Our project is heavily focused on implementation and empirical evidence, meaning that
while we did some analysis for how to structure our program, the most important thing
for us was to draw our own conclusions from experience that we have gotten through the
project.

\newpage
\tableofcontents
\newpage

\section{Problem Statement}
\begin{itemize}
\item Can we construct an infrastructure for benchmarking Futharks compilers, that will create ongoing reports of the development of a compilers quality?
  \begin{itemize}
  \item Can we run the benchmark tests everytime a new revision of Futhark is committed?
  \item Can we construct this infrastructure to work entirely without the need of human interaction?
  \end{itemize}
\end{itemize}

\section{Boundaries}
\begin{itemize}
\item We will not be able to reproduce earlier tests done by the infrastructure. We will not register driver versions or other environment details.
\item The benchmark tests will be run on the same provided server, and therefore we will not take server workload into consideration when running tests.
\item We will measure the performance of the tests in matters of run time and build time.
\end{itemize}

\section{Methods}
We will in this section describe what methods we have used during this project. It will contain methods for software development, and how we analysed our problem. \\ \\
Our methods for developing this project, has been agile software development, in which we sit down together typically 2 times a week and work on implementing tasks for our project. We haven't made smaller sub-tasks for our tasks, as we keep changing what the smaller components should do and how they work, therefore we consider a task done, when the product can perform the given task. \\ \\
For this project, there isn't a lot of past experiences that we can use. There isn't one specific way to create a benchmark infrastructure and there is no right or wrong way, so we use empirical evidence from observations and experimentation. These empirical experiences form the shape of our product. So our methods for getting these experiences have been researching how other organizations present benchmark results, and by implementing the benchmarking tools.

\section{Analysis}
In this section we will write down the analysis we have done through the project. The section will contain an analysis of units, as we were not sure how to measure the benchmarking. We also wanted to get inspiration on how to present the benchmarking results to the viewer. \\ \\
As mentioned in the methods section, most of our analysis have been empirical. We therefore haven't been able to analyse a specific solution for our problem, but rather gathered experiences that we can use to make a final product. As Futhark is a compiler we determined it was best to research other compilers, we mainly focused on GCC and GHC. \\
An organization called \textit{Linuxbenchmarking} run daily benchmarks on different compilers, most interesting for us is the daily GCC benchmarks. These tests run every morning on the latest GCC trunk code. The build is tested with different language such as C, C++ and Fortran. The organization uses \textit{Phoronix Test Suite} to scheduele and run the benchmarks. \footnote{Linux benchmarking, Daily GCC Benchmarks}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.33]{benchmarkgcc.png}
	 \caption{Benchmark results for a timed PHP compilation}
	 \label{fig:gccbench}
\end{figure}
~\\
As shown on figure \ref{fig:gccbench} we can see how the benchmark results are being displayed to the user. We can clearly see when the testing occurred, what the name of the test is and what the unit is. We can also see the values for each CPU, as they use different CPUs to show how the compilation is running.

\subsection{Units}
Through our research we have gathered that the most common and relevant units for benchmarking are:
\begin{enumerate}
\item Build/Run time
\item Instructions per second
\item Clock cycles
\item MFLOPs (Meta Floating-point Operations Per Second)
\end{enumerate}

Other units were also shown, but frames per second, requests per second and others are not relevant for our project.

\section{Tools}
We use Python 3.x for the implementation of the benchmarking infrastructure. The static webpages are HTML, but created with Python. We will use Github API to link the Futhark repository to our product. We will use a Python 2D plotting library called matplotlib for creating 2D graphs. The benchmarking will run on a provided Linux server called \textit{Napoleon}.\\

\section{Implementation}
Our program consists of multiple components, that have specific tasks to do. We will in this part of the report, go through these components, explaining our design decisions. Other than the components below, we have also built a small module, which consists mainly of expansions to existing Python functions, which main purpose is to help us do certain tasks in a more desireable way, this includes writing and reading files, creating and deleting folders, and even temporarily change directory.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{abstraction.png}
	 \caption{Abstract diagram over the infrastructure}
	 \label{fig:abstraction}
\end{figure}

Figure \ref{fig:abstraction} shows how our infrastructure works. We have a listener that waits for whenever a new revision of Futhark is pushed to GitHub. The listener then starts the whole benchmark process, by cloning the newest revision of Futhark, run all the tests that needs to be run, parse the output from the tests and create webpages and graphs from this output.

\subsection{Listener}
The listener is in charge of listening for GitHub webhook payloads that indicate that a new revision has been pushed to GitHub. This will be the program that runs as a daemon on a server and is in charge of putting the current revision into a queue to be benchmarked.

\subsection{Parsing}
\label{sec:parsing}
We have built a module to extract information from the Futhark repository. The most important tasks from this component is to find all the compilers that Futhark has, getting the results from tests that have been benchmarked and finding out which compilers each test is supposed to run on. \\ \\
It might not always be interesting to see how a specific test runs on certain compilers, there might also be periods in which a test should never be benchmarked. We have therefore developed a method to tell our program which compilers to run a benchmark on. Futhark tests already have a tag field put in comments to parse various information, and we decided to expand this tag field to also contain which compilers should be used to benchmark the test. The tests will therefore have a tag field as such:
\begin{lstlisting}
-- tags { futhark-c futhark-py AA BB CC }
\end{lstlisting}
We can tell from these tags, that this test should be run with the compilers futhark-c and futhark-py. With AA, BB, and CC being tags used by Futhark for other information. They will be ignored by this program.\\
\\
For parsing test results, we had to examine how Futhark shows results of its tests. An example of a test result is shown below:
\begin{lstlisting}
dataset data/trivial.in: 36412 36657 36612 36618 ... 36113
dataset data/100.in: 359040 359684 359416 360337 ... 359035
dataset data/kdd_cup.in: 1403769 1393373 1400563 ... 1400425
\end{lstlisting}
Each line states which dataset has been run for the test, followed by a specific number of runs. Each number for these runs represents how long it took to complete the test in nanoseconds. In this case we run each of the three datasets 10 times. Our parsing module has to get each of these values out, but also their corresponding dataset and of course whether or not the test was actually successful (see \hyperref[sec:benchmarking]{\ref*{sec:benchmarking} \nameref*{sec:benchmarking}}). We do this by reading the result files created by the benchmarking module and matching all its content with regular expression operations. We then store the data in a list with the following format: [Return code, [Dataset1, run1, run2, ..., run10], ..., [Dataset3, run1, run2, ..., run10]] \\ \\
This module is especially important to work correctly, because it has to be used for the web page generator.

\subsection{Benchmarking}
\label{sec:benchmarking}
The benchmarking module handles the actual benchmarking. It will use the parsing module to find out which compilers to benchmark (by parsing the Futhark test file) and will also take care of optional parameters such as number of runs per dataset. It will use the benchmarking tool included with Futhark called \texttt{futhark-bench} and write the results to a file. The result files will have the following format:
\begin{lstlisting}
0
dataset data/trivial.in: 36412 36657 36612 36618 ... 36113
dataset data/100.in: 359040 359684 359416 360337 ... 359035
dataset data/kdd_cup.in: 1403769 1393373 1400563 ... 1400425
\end{lstlisting}
The first line tells us that the exit code was 0, meaning that the test was successful where anything else would mean that an error had occurred. The rest was explained under \hyperref[sec:parsing]{\ref*{sec:parsing} Parsing}

\subsection{Web page Generator}
The primary task of the web page generator is to take the information from the parsing module and then create static web pages for each test, showing the results in the form of raw data and in the form of a graph. The secondary task is to show an overview of each compiler's quality.

\section{Discussion and reflection}
Through our analysis and dicussion with our counselor we figured that a single unit for measuring the quality of a compiler, would be for the best. As it would be easier to compare previous versions of a compiler with a newer version. We also figured that for consistency that build/run time would be the best unit for this. The benchmark results of each dataset returns each runs in microseconds ($\mu s$). 

\section{Conclusion}
Through our analysis and discussion, we have concluded that, using a single unit to show the overall quality of a compiler, the unit that fits best is runtime.

\section{Status}
In our synopsis we described the 5 major tasks that we have to implement in order for us to have a finalized product for this project. The tasks are: \\
\begin{enumerate}
\item Automatically download newest revision of Futhark.
\item Automatically run tests on the newest revision of Futhark
\item Benchmark data/result storage
\item Visual representation of benchmarking
\item Webpage generator
\end{enumerate}
Of this list, the first 3 tasks are almost complete, however there is still some implementation left in those 3 tasks. As we still need to figure out how to decide when to download the newest revision of Futhark, task \#2 and \#3 are almost complete. We haven't worked on task \#4 yet, but it is something we plan on doing soon. We have an idea of how to do task \#5 and have done a bit of implementation on this.

\newpage

\section{Bibliography}
\begin{itemize}
\item GGC, \textit{Benchmarking GCC}, Webpage, Seen: 2. April 2016, Site: \url{https://gcc.gnu.org/benchmarks/}
\item Linux Benchmarking, \textit{Daily GCC Benchmarks}, Webpage, Seen: 2. April 2016, Site: \url{http://www.linuxbenchmarking.com/?daily-gcc-benchmarks}
\item Tux, \textit{GNU/Linux Benchmarking - Practical aspects}, Webpage, Seen: 2. April 2016, Site: \url{http://www.tux.org/~balsa/linux/benchmarking/articles/html/Article2c.html}
\end{itemize}
\end{document}
