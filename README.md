Velkommen til Bachelor Project: Benchmark Infrastructure.

#Setup
## Dependencies
The program has the following dependencies needed to run and function properly:

* Python 3.x
    * gitpython
    * lxml
    * matplotlib
* stack - for building and installing the Futhark compilers

Furthermore, the environment variable $HOME needs to be set as **stack** will install the Futhark compiler binaries in $HOME/.local/bin.

Måske relevant materiale:

* [GCC Benchmark](https://gcc.gnu.org/benchmarks/)
* [LLVM vs GCC Performance](http://llvm.org/devmtg/2014-10/Slides/Molloy-LLVM-Performant-As-GCC-llvm-dev-2014.pdf)
* [GHC Performance](https://ghc.haskell.org/trac/ghc/wiki/CompilerPerformance)
* [GHC Infrastructure](https://ghc.haskell.org/trac/ghc/wiki/Infrastructure)
* [OpenBenchmarking.org](https://openbenchmarking.org/)
* [GCC Benchmarks Tests](http://www.linuxbenchmarking.com/?daily-gcc-benchmarks)
* [Compiler Comparisons](http://www.nersc.gov/users/computational-systems/retired-systems/hopper/performance-and-optimization/compiler-comparisons/)
* [GNU/Linux Benchmarking - Practical Aspects](http://www.tux.org/~balsa/linux/benchmarking/articles/html/Article2c.html#toc4)

Benchmark værdier:

* Instructions per second
* Build/Run time in seconds
* Cycles per byte (Indicates number of clock cycles a microprocessor will perform per byte)
* Clock Cycles (Total?)
* MFLOPs (Mega Floating-point Operations Per Second)
* Iterations Per Minute

Python Frameworks/Libraries:

* [Pandas](http://pandas.pydata.org/)
* [ggplot](http://ggplot.yhathq.com/)